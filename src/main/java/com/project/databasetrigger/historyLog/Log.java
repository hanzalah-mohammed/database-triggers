package com.project.databasetrigger.historyLog;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "log")
public class Log {

    @Id
    @Column(name = "log_id")
    private UUID logID;
    private String description;
    private String orderDetails;
    private LocalDateTime timestamp;

    public Log() {
    }

    public Log(UUID logID, String description, String orderDetails, LocalDateTime timestamp) {
        this.logID = logID;
        this.description = description;
        this.orderDetails = orderDetails;
        this.timestamp = timestamp;
    }

    public UUID getLogID() {
        return logID;
    }

    public void setLogID(UUID logID) {
        this.logID = logID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getDate() {
        return timestamp;
    }

    public void setDate(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(String orderDetails) {
        this.orderDetails = orderDetails;
    }

    @Override
    public String toString() {
        return "Log{" +
                "logID=" + logID +
                ", description='" + description + '\'' +
                ", order details='" + orderDetails + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
