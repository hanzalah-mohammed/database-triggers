package com.project.databasetrigger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatabaseTriggerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatabaseTriggerApplication.class, args);
	}

}
